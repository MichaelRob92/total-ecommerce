﻿using System;
using System.Linq;
using Total.Entity;

namespace Total.Framework.Extensions
{
    public static class ProductExtensions
    {
        public static bool IsValid(this Product product)
        {
            if (product.Deleted || product.Status == ProductStatus.Hidden)
                return false;

            if (product.Status != ProductStatus.TimeLimited)
                return true;

            var currentDate = DateTime.Now;
            var startDate = product.StartDate;
            var finishDate = product.FinishDate;

            if (startDate.HasValue && startDate.Value > currentDate)
                return false;

            return !finishDate.HasValue || finishDate.Value >= currentDate;
        }

        public static ProductMeta GetMeta(this Product product, ProductMetaKey key)
        {
            if (product.ProductMeta == null)
                throw new Exception("Product Meta not loaded, please Include(p => p.ProductMeta)");

            var meta = product.ProductMeta.FirstOrDefault(e => e.Key == key);
            return meta;
        }

        public static string GetMetaValue(this Product product, ProductMetaKey key)
        {
            if (product.ProductMeta == null)
                throw new Exception("Product Meta not loaded, please Include(p => p.ProductMeta)");

            var meta = product.ProductMeta.FirstOrDefault(e => e.Key == key);
            if (meta == null)
                throw new Exception("Product Meta cannot be null.");
            return meta.Value;
        }

        public static string TryGetMetaValue(this Product product, ProductMetaKey key, string @default)
        {
            if (product.ProductMeta == null)
                throw new Exception("Product Meta not loaded, please Include(p => p.ProductMeta)");

            var meta = product.ProductMeta.FirstOrDefault(e => e.Key == key);
            return meta != null ? meta.Value : @default;
        }
    }
}
