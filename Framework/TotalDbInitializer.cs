﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Total.Entity;

namespace Total.Framework
{
    public class TotalDbInitializer : CreateDatabaseIfNotExists<TotalDbContext>
    {
        protected override void Seed(TotalDbContext context)
        {
            var user = new User {Id = Guid.NewGuid(), Email = "michael.robinson@totalsolutionsgroup.co.uk", EmailConfirmed = true, SecurityStamp = Guid.NewGuid().ToString(), PhoneNumberConfirmed = false, TwoFactorEnabled = false, LockoutEnabled = false, AccessFailedCount = 0, UserName = "michael.robinson@totalsolutionsgroup.co.uk"};
            context.Users.Add(user);
            context.SaveChanges();

            var brand = new Brand {Status = BrandStatus.Live, Name = "Total", Description = "Total Brand", Content = "Total Brand", SortOrder = 0, Image = "total-brand.jpg", ImageAlt = "Total Brand", CreatedById = user.Id, CreatedDateTime = DateTime.Now, Deleted = false};
            context.Brands.Add(brand);
            context.SaveChanges();

            var random = new Random();
            for (var i = 1; i < 12; i++)
            {
                var product = new Product {BrandId = brand.Id, Status = ProductStatus.Live, Name = String.Format("Example Product {0}", i), Code = String.Format("{0:0000}", i), Summary = "Example Product", Content = "Example Product", Price = random.Next(20, 200), SpecialOffer = false, SortOrder = i, Discontinued = false, CreatedById = user.Id, CreatedDateTime = DateTime.Now, Deleted = false};
                context.Products.Add(product);
                context.SaveChanges();

                product.ProductMeta = new List<ProductMeta> {new ProductMeta {Key = ProductMetaKey.Permalink, Value = String.Format("example-product-{0}", i)}};
                product.ProductMedia = new List<ProductMedia> {new ProductMedia {SortOrder = 0, Type = ProductMediaType.Image, Value = "product.jpg"}};
                context.SaveChanges();
            }

            base.Seed(context);
        }
    }
}
