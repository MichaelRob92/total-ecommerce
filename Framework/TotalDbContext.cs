﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using Total.Entity;

namespace Total.Framework
{
    public class TotalDbContext : IdentityDbContext<User, Role, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public TotalDbContext() : base("TotalCommerce")
        {
            Database.SetInitializer(new TotalDbInitializer());
            RequireUniqueEmail = true;
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual IDbSet<Address> Addresses { get; set; }
        public virtual IDbSet<Entity.Attribute> Attributes { get; set; }
        public virtual IDbSet<AttributeValue> AttributeValues { get; set; }
        public virtual IDbSet<Banner> Banners { get; set; } 
        public virtual IDbSet<Basket> Baskets { get; set; }
        public virtual IDbSet<BasketProduct> BasketProducts { get; set; }
        public virtual IDbSet<Brand> Brands { get; set; }
        public virtual IDbSet<BrandMeta> BrandMeta { get; set; }
        public virtual IDbSet<Category> Categories { get; set; }
        public virtual IDbSet<CategoryMeta> CategoryMeta { get; set; }
        public virtual IDbSet<CategoryProductAssociation> CategoryProductAssociations { get; set; }
        public virtual IDbSet<Courier> Couriers { get; set; }
        public virtual IDbSet<MailingList> MailingLists { get; set; }
        public virtual IDbSet<Member> Members { get; set; }
        public virtual IDbSet<MemberMeta> MemberMeta { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<OrderItem> OrderItems { get; set; }
        public virtual IDbSet<Page> Pages { get; set; }
        public virtual IDbSet<PageMeta> PageMeta { get; set; }
        public virtual IDbSet<Product> Products { get; set; }
        public virtual IDbSet<ProductAttribute> ProductAttributes { get; set; }
        public virtual IDbSet<ProductMedia> ProductMedia { get; set; } 
        public virtual IDbSet<ProductMeta> ProductMeta { get; set; }
        public virtual IDbSet<ProductVariation> ProductVariations { get; set; }
        public virtual IDbSet<Promotion> Promotions { get; set; }
        public virtual IDbSet<RelatedProduct> RelatedProducts { get; set; }
        public virtual IDbSet<Transaction> Transactions { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<GuidUserRole>().ToTable("UserRole");
            modelBuilder.Entity<GuidUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<GuidUserClaim>().ToTable("UserClaim");
            
            modelBuilder.Entity<User>().HasOptional(e => e.Member).WithRequired(e => e.User);

            modelBuilder.Entity<Order>().HasOptional(e => e.Transaction).WithRequired(e => e.Order);

            // User cascade
            modelBuilder.Entity<Banner>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Brand>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Category>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Courier>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Page>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Product>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<Promotion>().HasRequired(e => e.CreatedBy).WithMany().WillCascadeOnDelete(false);

            // Product cascade
            modelBuilder.Entity<RelatedProduct>().HasRequired(e => e.SecondaryProduct).WithMany().WillCascadeOnDelete(false);
        }
    }
}
