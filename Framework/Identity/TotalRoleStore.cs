﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Total.Entity;

namespace Total.Framework.Identity
{
    public class TotalRoleStore : RoleStore<Role, Guid, GuidUserRole>
    {
        public TotalRoleStore(DbContext context) : base(context)
        {
        }
    }
}
