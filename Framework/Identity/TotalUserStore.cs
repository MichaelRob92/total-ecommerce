﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Total.Entity;

namespace Total.Framework.Identity
{
    public class TotalUserStore : UserStore<User, Role, Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public TotalUserStore() : base(new TotalDbContext())
        {
        }

        public TotalUserStore(DbContext context) : base(context)
        {
        }
    }
}
