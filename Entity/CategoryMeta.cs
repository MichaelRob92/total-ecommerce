﻿namespace Total.Entity
{
    public class CategoryMeta : Meta
    {
        public int CategoryId { get; set; }

        public CategoryMetaKey Key { get; set; }

        public virtual Category Category { get; set; }
    }

    public enum CategoryMetaKey
    {
        Permalink, MetaTitle, MetaDescription
    }
}
