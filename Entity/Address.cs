﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class Address
    {
        public int Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(128)]
        public string Label { get; set; }

        [Required]
        [StringLength(12)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(64)]
        public string LastName { get; set; }

        [StringLength(64)]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public string BuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public string AddressLine1 { get; set; }

        [StringLength(64)]
        public string AddressLine2 { get; set; }

        [StringLength(64)]
        public string AddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public string TownCity { get; set; }

        [Required]
        [StringLength(64)]
        public string County { get; set; }

        [Required]
        [StringLength(64)]
        public string Postcode { get; set; }

        [Required]
        [StringLength(64)]
        public string Country { get; set; }

        [Required]
        [StringLength(16)]
        public string Telephone { get; set; }

        [StringLength(16)]
        public string Mobile { get; set; }

        [Required]
        [StringLength(256)]
        public string Email { get; set; }

        [StringLength(512)]
        public string Instructions { get; set; }

        public bool Default { get; set; }

        public virtual User User { get; set; }
    }
}
