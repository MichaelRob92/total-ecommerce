﻿using System;

namespace Total.Entity
{
    public class MemberMeta : Meta
    {
        public Guid MemberUserId { get; set; }

        public MemberMetaKey Key { get; set; }

        public virtual Member Member { get; set; }
    }

    public enum MemberMetaKey
    {
    }
}
