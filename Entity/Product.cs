﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class Product
    {
        public int Id { get; set; }

        public int BrandId { get; set; }

        public ProductStatus Status { get; set; }

        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FinishDate { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        
        [StringLength(32)]
        public string Code { get; set; }

        [Required]
        [StringLength(128)]
        public string Summary { get; set; }

        [Required]
        public string Content { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal? OfferPrice { get; set; }

        public bool SpecialOffer { get; set; }

        [StringLength(512)]
        public string SpecialOfferMessage { get; set; }

        public decimal? Weight { get; set; }

        public int SortOrder { get; set; }

        public bool Discontinued { get; set; }

        [StringLength(512)]
        public string DiscontinuedMessage { get; set; }

        public bool Featured { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid? LastModifiedById { get; set; }

        public DateTime? LastModifiedDateTime { get; set; }

        public Guid? DeletedById { get; set; }

        public DateTime? DeletedDateTime { get; set; }

        public bool Deleted { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }

        public virtual ICollection<ProductMedia> ProductMedia { get; set; } 

        public virtual ICollection<ProductMeta> ProductMeta { get; set; }

        public virtual ICollection<ProductVariation> ProductVariations { get; set; }

        public virtual ICollection<CategoryProductAssociation> CategoryProductAssociations { get; set; }

        public virtual ICollection<RelatedProduct> RelatedProducts { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; } 
    }

    public enum ProductStatus
    {
        Live, TimeLimited, Hidden
    }
}
