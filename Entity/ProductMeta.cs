﻿namespace Total.Entity
{
    public class ProductMeta : Meta
    {
        public int ProductId { get; set; }

        public ProductMetaKey Key { get; set; }

        public virtual Product Product { get; set; }
    }

    public enum ProductMetaKey
    {
        Permalink, MetaTitle, MetaDescription
    }
}
