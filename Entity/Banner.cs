﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class Banner
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Image { get; set; }

        [Required]
        [StringLength(256)]
        public string ImageAlt { get; set; }

        public BannerPosition Position { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid? LastModifiedById { get; set; }

        public DateTime? LastModifiedDateTime { get; set; }

        public Guid? DeletedById { get; set; }

        public DateTime? DeletedDateTime { get; set; }

        public bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }
    }

    public enum BannerPosition
    {
        MainFeature, LeftFeature, RightFeature
    }
}
