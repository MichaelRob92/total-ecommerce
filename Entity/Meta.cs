﻿using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public abstract class Meta
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Value { get; set; }
    }
}
