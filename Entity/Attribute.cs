﻿using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class Attribute
    {
        public int Id { get; set; }

        public AttributeType Type { get; set; }

        public AttributeStyle Style { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }

    public enum AttributeType
    {
        Standard, BuyingOption
    }

    public enum AttributeStyle
    {
        List, SelectList, RadioList, CheckboxList, ImageGrid
    }
}
