﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class Member
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(12)]
        public string Title { get; set; }

        [Required]
        [StringLength(64)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(64)]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }

        public virtual ICollection<MemberMeta> MemberMeta { get; set; }

        public virtual User User { get; set; }
    }
}
