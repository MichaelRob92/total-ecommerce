﻿using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class AttributeValue
    {
        public int Id { get; set; }

        public int AttributeId { get; set; }

        [Required]
        [StringLength(256)]
        public string Value { get; set; }

        [StringLength(256)]
        public string Image { get; set; }

        [StringLength(256)]
        public string ImageAlt { get; set; }

        public int SortOrder { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
