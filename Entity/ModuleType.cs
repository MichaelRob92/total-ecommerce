﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class ModuleType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Label { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public int MenuPosition { get; set; }

        [Required]
        [StringLength(128)]
        public string Permalink { get; set; }

        public virtual ICollection<ModuleLabel> Labels { get; set; }

        public virtual ICollection<ModuleCapability> Capabilities { get; set; }
    }

    public enum ModuleCapabilityType
    {
        Public, ExcludeFromSearch, PubliclyQueryable, ShowUi, ShowInNavMenus, ShowInMenu
    }

    public enum ModuleLabelType
    {
        Name, SingularName, MenuName, AllItems, AddNew, AddNewItem, EditItem, NewItem, ViewItem, SearchItems, NotFound, NotFoundInTrash
    }
}
