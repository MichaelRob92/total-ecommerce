﻿namespace Total.Entity
{
    public class ModuleLabel
    {
        public int Id { get; set; }

        public int ModuleTypeId { get; set; }

        public ModuleLabelType Type { get; set; }

        public string Value { get; set; }

        public virtual ModuleType ModuleType { get; set; }
    }
}
