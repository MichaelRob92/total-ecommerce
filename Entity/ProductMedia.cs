﻿using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class ProductMedia
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public ProductMediaType Type { get; set; }

        [Required]
        [StringLength(256)]
        public string Value { get; set; }

        public int SortOrder { get; set; }

        public virtual Product Product { get; set; }
    }

    public enum ProductMediaType
    {
        Image, Video
    }
}
