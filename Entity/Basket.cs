﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class Basket
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public Guid? UserId { get; set; }

        [Required]
        [StringLength(16)]
        public string IpAddress { get; set; }

        [Required]
        [StringLength(32)]
        public string SessionId { get; set; }

        public virtual ICollection<BasketProduct> BasketProducts { get; set; }
    }
}
