﻿namespace Total.Entity
{
    public class BrandMeta : Meta
    {
        public int BrandId { get; set; }

        public BrandMetaKey Key { get; set; }

        public virtual Brand Brand { get; set; }
    }

    public enum BrandMetaKey
    {
        Permalink, MetaTitle, MetaDescription
    }
}
