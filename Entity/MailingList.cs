﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class MailingList
    {
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string EmailAddress { get; set; }

        public Guid? UserId { get; set; }

        public virtual User User { get; set; }
    }
}
