﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Total.Entity
{
    public class User : IdentityUser<Guid, GuidUserLogin, GuidUserRole, GuidUserClaim>
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(string name) : this()
        {
            UserName = name;
        }

        public virtual Member Member { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
    
    public class GuidUserLogin : IdentityUserLogin<Guid>
    {
        
    }

    public class GuidUserRole : IdentityUserRole<Guid>
    {

    }

    public class GuidUserClaim : IdentityUserClaim<Guid>
    {
        
    }
}
