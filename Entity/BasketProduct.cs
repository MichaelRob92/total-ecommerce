﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class BasketProduct
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid BasketId { get; set; }

        public int ProductId { get; set; }

        public int? ProductVariationId { get; set; }

        public int Quantity { get; set; }

        public virtual Basket Basket { get; set; }

        public virtual Product Product { get; set; }

        public virtual ProductVariation ProductVariation { get; set; }
    }
}
