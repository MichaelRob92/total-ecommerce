﻿namespace Total.Entity
{
    public class ModuleCapability
    {
        public int Id { get; set; }

        public int ModuleTypeId { get; set; }

        public ModuleCapabilityType Type { get; set; }

        public bool Value { get; set; }

        public virtual ModuleType ModuleType { get; set; }
    }
}
