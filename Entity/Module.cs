﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class Module
    {
        public int Id { get; set; }

        public int ModuleTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Summary { get; set; }

        public string Content { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid? LastModifiedById { get; set; }

        public DateTime? LastModifiedDateTime { get; set; }

        public Guid? DeletedById { get; set; }

        public DateTime? DeletedDateTime { get; set; }

        public bool Deleted { get; set; }

        public virtual IEnumerable<ModuleMeta> ModuleMeta { get; set; }

        public virtual ModuleType ModuleType { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }
    }
}
