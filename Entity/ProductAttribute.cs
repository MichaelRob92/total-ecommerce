﻿namespace Total.Entity
{
    public class ProductAttribute
    {
        public int Id { get; set; }

        public int AttributeId { get; set; }

        public bool Required { get; set; }

        public virtual Attribute Attribute { get; set; }
    }
}
