﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class Transaction
    {
        public int Id { get; set; }

        public Guid OrderId { get; set; }

        [Required]
        [StringLength(128)]
        public string UniqueCode { get; set; }

        [Required]
        [StringLength(16)]
        public string IpAddress { get; set; }

        [Required]
        [StringLength(32)]
        public string SessionId { get; set; }

        public DateTime TransactionDateTime { get; set; }

        [Required]
        public string EncryptionString { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal NetAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal TaxAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal DeliveryAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal GrossAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal AuthorisedAmount { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal? DiscountAmount { get; set; }

        [Required]
        [StringLength(50)]
        public string Currency { get; set; }

        public bool Authorised { get; set; }

        [StringLength(128)]
        public string AuthorisationCode { get; set; }

        public DateTime? AuthorisedDateTime { get; set; }

        public bool ManuallyAuthorised { get; set; }

        public Guid? ManuallyAuthorisedById { get; set; }

        public virtual Order Order { get; set; }

        public virtual User ManuallyAuthorisedBy { get; set; }
    }
}
