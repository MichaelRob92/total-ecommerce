﻿namespace Total.Entity
{
    public class ModuleMeta : Meta
    {
        public int ModuleId { get; set; }

        public ModuleMetaKey Key { get; set; }

        public virtual Module Module { get; set; }
    }

    public enum ModuleMetaKey
    {
        Permalink, MetaTitle, MetaDescription
    }
}
