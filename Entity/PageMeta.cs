﻿namespace Total.Entity
{
    public class PageMeta : Meta
    {
        public int PageId { get; set; }

        public PageMetaKey Key { get; set; }

        public virtual Page Page { get; set; }
    }

    public enum PageMetaKey
    {
        Permalink, MetaTitle, MetaDescription
    }
}
