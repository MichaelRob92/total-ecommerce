﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Total.Entity
{
    public class Brand
    {
        public int Id { get; set; }

        public BrandStatus Status { get; set; }

        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? FinishDate { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public string Content { get; set; }

        public int SortOrder { get; set; }

        [StringLength(256)]
        public string Image { get; set; }

        [StringLength(256)]
        public string ImageAlt { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public Guid? LastModifiedById { get; set; }

        public DateTime? LastModifiedDateTime { get; set; }

        public Guid? DeletedById { get; set; }

        public DateTime? DeletedDateTime { get; set; }

        public bool Deleted { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User LastModifiedBy { get; set; }

        public virtual User DeletedBy { get; set; }

        public virtual ICollection<BrandMeta> BrandMeta { get; set; }
    }

    public enum BrandStatus
    {
        Live, TimeLimited, Hidden
    }
}
