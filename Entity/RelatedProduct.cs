﻿namespace Total.Entity
{
    public class RelatedProduct
    {
        public int Id { get; set; }

        public RelatedProductType Type { get; set; }

        public int PrimaryProductId { get; set; }

        public int SecondaryProductId { get; set; }

        public virtual Product PrimaryProduct { get; set; }

        public virtual Product SecondaryProduct { get; set; }
    }

    public enum RelatedProductType
    {
        UpSell, CrossSell
    }
}
