﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Total.Entity
{
    public class ProductVariation
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int ProductAttributeId { get; set; }

        public int AttributeValueId { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal? PriceModifier { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal? DeliveryModifier { get; set; }

        public decimal? WeightModifier { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal? VatModifier { get; set; }

        public bool Active { get; set; }

        public virtual ProductAttribute ProductAttribute { get; set; }
        
        public virtual Product Product { get; set; }
    }
}
