﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Total.Entity
{
    public class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public Guid? UserId { get; set; }

        [DataType(DataType.Date)]
        public DateTime? RequestedDeliveryDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EstimatedDeliveryDate { get; set; }

        public string DispatchNote { get; set; }

        public Guid? SignedOffById { get; set; }

        public DateTime? SignedOffDate { get; set; }

        public int? CourierId { get; set; }

        [StringLength(32)]
        public string CourierTrackingReference { get; set; }

        public OrderStatus Status { get; set; }

        [StringLength(1024)]
        public string StatusMessage { get; set; }

        [Required]
        [StringLength(12)]
        public string BillingTitle { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingFirstName { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingLastName { get; set; }

        [StringLength(64)]
        public string BillingCompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingBuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingAddressLine1 { get; set; }

        [StringLength(64)]
        public string BillingAddressLine2 { get; set; }

        [StringLength(64)]
        public string BillingAddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingTownCity { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingCounty { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingPostcode { get; set; }

        [Required]
        [StringLength(64)]
        public string BillingCountry { get; set; }

        [Required]
        [StringLength(16)]
        public string BillingTelephone { get; set; }

        [StringLength(16)]
        public string BillingMobile { get; set; }

        [Required]
        [StringLength(256)]
        public string BillingEmail { get; set; }

        [Required]
        [StringLength(12)]
        public string DeliveryTitle { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryFirstName { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryLastName { get; set; }

        [StringLength(64)]
        public string DeliveryCompanyName { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryBuildingAddress { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryAddressLine1 { get; set; }

        [StringLength(64)]
        public string DeliveryAddressLine2 { get; set; }

        [StringLength(64)]
        public string DeliveryAddressLine3 { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryTownCity { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryCounty { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryPostcode { get; set; }

        [Required]
        [StringLength(64)]
        public string DeliveryCountry { get; set; }

        [Required]
        [StringLength(16)]
        public string DeliveryTelephone { get; set; }

        [StringLength(16)]
        public string DeliveryMobile { get; set; }

        [Required]
        [StringLength(256)]
        public string DeliveryEmail { get; set; }

        [StringLength(512)]
        public string DeliveryInstructions { get; set; }

        public virtual User User { get; set; }

        public virtual User SignedOffBy { get; set; }

        public virtual Courier Courier { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual Transaction Transaction { get; set; }

    }

    public enum OrderStatus
    {
        Pending, Paid, Dispatched
    }
}
