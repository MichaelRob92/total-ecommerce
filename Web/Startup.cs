﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Total.Web.Startup))]
namespace Total.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
