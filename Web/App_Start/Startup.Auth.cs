﻿using System.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Total.Web
{
	public partial class Startup
	{
	    public void ConfigureAuth(IAppBuilder app)
	    {

	        var cookieName = ConfigurationManager.AppSettings["ClientName"].Replace(" ", "");

	        app.UseCookieAuthentication(new CookieAuthenticationOptions
	        {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                CookieName = cookieName,
				ReturnUrlParameter = "ref",
                LoginPath = new PathString("/account/login"),
                LogoutPath = new PathString("/account/logout")
	        });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseGoogleAuthentication("163085464537-8n09tob9i3lrh6skpgcsedgkuql9mctu.apps.googleusercontent.com", "PtXHg-9jg5_TmYoF0JGCZ0Wh");
	    }
	}
}