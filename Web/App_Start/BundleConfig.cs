﻿using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

namespace Total.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            var builder = new NullBuilder();
            var orderer = new NullOrderer();
            var cssTransformer = new CssTransformer();
            var jsTransformer = new JsTransformer();

            var bootstrapStyleBundle = new Bundle("~/css/bootstrap").Include("~/assets/less/bootstrap/bootstrap.less");
            bootstrapStyleBundle.Builder = builder;
            bootstrapStyleBundle.Orderer = orderer;
            bootstrapStyleBundle.Transforms.Add(cssTransformer);
            bundles.Add(bootstrapStyleBundle);

            var fontAwesomeBundle = new Bundle("~/css/font-awesome").Include("~/assets/less/font-awesome/font-awesome.less");
            fontAwesomeBundle.Builder = builder;
            fontAwesomeBundle.Orderer = orderer;
            fontAwesomeBundle.Transforms.Add(cssTransformer);
            bundles.Add(fontAwesomeBundle);

            var loginStylesBundle = new Bundle("~/css/login");
            loginStylesBundle.Include("~/assets/less/ecommerce/login.less");
            loginStylesBundle.Builder = builder;
            loginStylesBundle.Transforms.Add(cssTransformer);
            loginStylesBundle.Orderer = orderer;
            bundles.Add(loginStylesBundle);

            var mainStylesBundle = new Bundle("~/css/main");
            mainStylesBundle.Include("~/assets/less/ecommerce/ecommerce.less");
            mainStylesBundle.Builder = builder;
            mainStylesBundle.Transforms.Add(cssTransformer);
            mainStylesBundle.Orderer = orderer;
            bundles.Add(mainStylesBundle);

            var modernizrBundle = new Bundle("~/js/modernizr").Include("~/assets/js/vendor/modernizr-2.*");
            modernizrBundle.Builder = builder;
            modernizrBundle.Orderer = orderer;
            modernizrBundle.Transforms.Add(jsTransformer);
            bundles.Add(modernizrBundle);

            var jQueryBundle = new Bundle("~/js/jquery", "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js").Include("~/assets/js/vendor/jquery-1.11.0.min.js");
            jQueryBundle.Builder = builder;
            jQueryBundle.Orderer = orderer;
            jQueryBundle.Transforms.Add(jsTransformer);
            jQueryBundle.CdnFallbackExpression = "window.jQuery";
            bundles.Add(jQueryBundle);

            var bootstrapScriptBundle = new Bundle("~/js/bootstrap").Include(
                // Bootstrap
                "~/assets/js/bootstrap/affix.js",
                "~/assets/js/bootstrap/alert.js",
                "~/assets/js/bootstrap/button.js",
                "~/assets/js/bootstrap/carousel.js",
                "~/assets/js/bootstrap/collapse.js",
                "~/assets/js/bootstrap/dropdown.js",
                "~/assets/js/bootstrap/tab.js",
                "~/assets/js/bootstrap/transition.js",
                "~/assets/js/bootstrap/scrollspy.js",
                "~/assets/js/bootstrap/modal.js",
                "~/assets/js/bootstrap/tooltip.js",
                "~/assets/js/bootstrap/popover.js"
            );
            bootstrapScriptBundle.Builder = builder;
            bootstrapScriptBundle.Orderer = orderer;
            bootstrapScriptBundle.Transforms.Add(jsTransformer);
            bundles.Add(bootstrapScriptBundle);
        }
    }
}