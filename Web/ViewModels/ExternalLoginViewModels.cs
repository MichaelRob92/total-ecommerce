﻿namespace Total.Web.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {

    }

    public class ExternalLoginViewModel
    {
        public string Action { get; set; }
        public string ReturnUrl { get; set; }
    }
}