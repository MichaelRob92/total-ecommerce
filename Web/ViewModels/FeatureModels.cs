﻿using Total.Entity;

namespace Total.Web.ViewModels
{
    public class SideFeatureModel
    {
        public Banner LeftBanner { get; set; }
        public Banner RightBanner { get; set; }
    }
}