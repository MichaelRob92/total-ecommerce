﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Total.Framework;
using Total.Helpers;

namespace Total.Web.Areas.Products.Controllers
{
    [ChildActionOnly]
    public class PartialController : Controller
    {
        private TotalDbContext _context;

        public PartialController() : this(new TotalDbContext())
        {}

        public PartialController(TotalDbContext context)
        {
            _context = context;
        }

        public ActionResult FeaturedItems()
        {
            var products = _context.Products.Where(p => !p.Deleted && p.Featured).OrderBy(p => Guid.NewGuid()).Include(p => p.ProductMeta).Take(8).ChunkData(2);
            return View("SmallProductGrid", products);
        }

        public ActionResult BestSellers()
        {
            var products = _context.Products.Where(p => !p.Deleted).OrderBy(p => p.OrderItems.Sum(oi => oi.Quantity)).Include(p => p.ProductMeta).Take(2);
            return View("TinyProduct", products);
        }

        public ActionResult NewItems()
        {
            var products = _context.Products.Where(p => !p.Deleted).OrderByDescending(p => p.CreatedDateTime).Include(p => p.ProductMeta).Take(2);
            return View("TinyProduct", products);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
                _context = null;
            }
            base.Dispose(disposing);
        }
    }
}