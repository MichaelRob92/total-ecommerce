﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Total.Entity;
using Total.Framework;
using Total.Framework.Extensions;
using Total.Web.Areas.Products.ViewModels;

namespace Total.Web.Areas.Products.Controllers
{
    public class SingleController : Controller
    {
        private TotalDbContext _context;

        public SingleController() : this(new TotalDbContext())
        {}

        public SingleController(TotalDbContext context)
        {
            _context = context;
        }

        // GET: product/{permalink}-{id}
        public async Task<ActionResult> Index(string permalink, int id)
        {
            var product = await _context.Products.Include(p => p.ProductMeta).Include(p => p.ProductMedia).FirstOrDefaultAsync(p => p.Id.Equals(id));
            if (product == null || !product.IsValid())
                return Redirect("~/notfound");

            var media = product.ProductMedia.OrderBy(m => m.SortOrder);
            var defaultImage = media.First(m => m.Type == ProductMediaType.Image);
            return View(new ProductViewModel { Product = product, DefaultImage = defaultImage, Media = media });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
                _context = null;
            }
            base.Dispose(disposing);
        }
    }
}