﻿using System.Collections.Generic;
using Total.Entity;

namespace Total.Web.Areas.Products.ViewModels
{
    public class ProductViewModel
    {
        public Product Product { get; set; }
        public ProductMedia DefaultImage { get; set; }
        public IEnumerable<ProductMedia> Media { get; set; }
    }
}