﻿using System.Web.Mvc;

namespace Total.Web.Areas.Products
{
    public class ProductsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Products";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Products_Single",
                "product/{permalink}-{id}",
                new { controller = "Single", action = "Index" },
                new[] { "Total.Web.Areas.Products.Controllers" }
            );

            context.MapRoute(
                "Products_Default",
                "products/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "Total.Web.Areas.Products.Controllers" }
            );
        }
    }
}