﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Total.Entity;
using Total.Framework;
using Total.Framework.Identity;
using Total.Web.ViewModels;

namespace Total.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<User, Guid>(new TotalUserStore(new TotalDbContext())))
        { }

        public AccountController(UserManager<User, Guid> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<User, Guid> UserManager { get; private set; }

        //
        // GET: /account/login
        [AllowAnonymous]
        public ActionResult Login(string @ref)
        {
            ViewBag.ReturnUrl = @ref;
            return View();
        }

        //
        // POST: /account/login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string @ref)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await UserManager.FindAsync(model.EmailAddress, model.Password);
            if (user != null)
            {
                await SignInAsync(user, model.RememberMe);
                return RedirectToLocal(@ref);
            }

            ModelState.AddModelError("", "Invalid username or password.");
            return View(model);
        }

        //
        // POST: /account/logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return Redirect("~/");
        }

        //
        // POST: /account/external-login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ActionName("external-login")]
        public ActionResult ExternalLogin(string provider, string @ref)
        {
            return new ChallengeResult(provider, Url.Action("external-login-callback", "Account", new { @ref }));
        }

        //
        // GET: /account/external-login-callback
        [AllowAnonymous]
        [ActionName("external-login-callback")]
        public async Task<ActionResult> ExternalLoginCallback(string @ref)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
                return RedirectToAction("Login");
            
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, false);
                return RedirectToLocal(@ref);
            }

            ViewBag.ReturnUrl = @ref;
            ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
            return View("ExternalLoginConfirmation");
        }

        //
        // POST: /account/external-login-confirmation
        [HttpPost]
        [AllowAnonymous]
        [ActionName("external-login-confirmation")]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string @ref)
        {
            if (User.Identity.IsAuthenticated)
                return Redirect("~/");

            if (ModelState.IsValid)
            {
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                    return View("ExternalLoginFailure");

                var user = new User(info.Email) {Email = info.Email, EmailConfirmed = true};

                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, false);
                        return RedirectToLocal(@ref);
                    }
                }
                AddIdentityErrors(result);
            }

            ViewBag.ReturnUrl = @ref;
            return View("ExternalLoginConfirmation", model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent}, identity);
        }

        private void AddIdentityErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError("", error);
        }

        private ActionResult RedirectToLocal(string @ref)
        {
            return Redirect(Url.IsLocalUrl(@ref) ? @ref : "~/");
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            private readonly string _loginProvider;
            private readonly string _redirectUri;
            private readonly string _userId;

            public ChallengeResult(string provider, string redirectUri, string userId = null)
            {
                _loginProvider = provider;
                _redirectUri = redirectUri;
                _userId = userId;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties {RedirectUri = _redirectUri};
                if (_userId != null)
                    properties.Dictionary[XsrfKey] = _userId;

                var authenticationManager = context.HttpContext.GetOwinContext().Authentication;
                authenticationManager.Challenge(properties, _loginProvider);
            }
        }

        #endregion
    }
}