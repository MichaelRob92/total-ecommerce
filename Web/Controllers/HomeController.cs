﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Total.Entity;
using Total.Framework;
using Total.Web.ViewModels;

namespace Total.Web.Controllers
{
    public class HomeController : Controller
    {
        private TotalDbContext _context;

        public HomeController() : this(new TotalDbContext())
        {}

        public HomeController(TotalDbContext context)
        {
            _context = context;
        }

        //
        // GET: /
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult MainFeature()
        {
            var banners = _context.Banners.Where(b => !b.Deleted && b.Position == BannerPosition.MainFeature).Take(3);
            return View("Partials/_MainFeature", banners);
        }

        [ChildActionOnly]
        public ActionResult SideFeatures()
        {
            var leftBanner = _context.Banners.FirstOrDefault(b => !b.Deleted && b.Position == BannerPosition.LeftFeature);
            var rightBanner = _context.Banners.FirstOrDefault(b => !b.Deleted && b.Position == BannerPosition.RightFeature);

            return View("Partials/_SideFeatures", new SideFeatureModel { LeftBanner = leftBanner, RightBanner = rightBanner});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
                _context = null;
            }
            base.Dispose(disposing);
        }
	}
}