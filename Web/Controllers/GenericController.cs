﻿using System.Linq;
using System.Web.Mvc;
using Total.Framework;

namespace Total.Web.Controllers
{
    public class GenericController : Controller
    {

        private TotalDbContext _context;

        public GenericController() : this(new TotalDbContext())
        {}

        public GenericController(TotalDbContext context)
        {
            _context = context;
        }

        [ChildActionOnly]
        public ActionResult PrimaryNavigation()
        {
            var categories = _context.Categories.Where(c => !c.ParentCategoryId.HasValue).OrderBy(c => c.SortOrder);
            return View("Partials/_PrimaryNavigation", categories);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
                _context = null;
            }
            base.Dispose(disposing);
        }
    }
}