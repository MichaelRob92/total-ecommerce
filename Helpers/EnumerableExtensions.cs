﻿using System.Collections.Generic;
using System.Linq;

namespace Total.Helpers
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<TSource>> ChunkData<TSource>(this IEnumerable<TSource> source, int chunkSize)
        {
            var enumerable = source as IList<TSource> ?? source.ToList();
            var count = enumerable.Count();
            for (var i = 0; i < count; i += chunkSize)
                yield return enumerable.Skip(i).Take(chunkSize);
        }


        //class ChunkedEnumerable<T> : IEnumerable<T>
        //{
        //    class ChildEnumerator : IEnumerator<T>
        //    {
        //        private readonly ChunkedEnumerable<T> _parent;
        //        private int _position;
        //        private bool _done;
        //        private T _current;

        //        public ChildEnumerator(ChunkedEnumerable<T> parent)
        //        {
        //            _parent = parent;
        //            _position = -1;
        //            parent._wrapper.AddRef();
        //        }

        //        public T Current
        //        {
        //            get
        //            {
        //                if (_position == -1 || _done)
        //                    throw new InvalidOperationException();
        //                return _current;
        //            }
        //        }

        //        public void Dispose()
        //        {
        //            if (_done) return;
        //            _done = true;
        //            _parent._wrapper.RemoveRef();
        //        }

        //        object System.Collections.IEnumerator.Current
        //        {
        //            get { return Current; }
        //        }

        //        public bool MoveNext()
        //        {
        //            _position++;

        //            if (_position + 1 > _parent._chunkSize)
        //                _done = true;

        //            if (!_done)
        //                _done = !_parent._wrapper.Get(_position + _parent._start, out _current);

        //            return !_done;
        //        }

        //        public void Reset()
        //        {
        //            throw new NotSupportedException();
        //        }
        //    }

        //    private readonly EnumeratorWrapper<T> _wrapper;
        //    private readonly int _chunkSize;
        //    private readonly int _start;

        //    public ChunkedEnumerable(EnumeratorWrapper<T> wrapper, int chunkSize, int start)
        //    {
        //        _wrapper = wrapper;
        //        _chunkSize = chunkSize;
        //        _start = start;
        //    }

        //    public IEnumerator<T> GetEnumerator()
        //    {
        //        return new ChildEnumerator(this);
        //    }

        //    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        //    {
        //        return GetEnumerator();
        //    }
        //}

        //class EnumeratorWrapper<T>
        //{
        //    public EnumeratorWrapper(IEnumerable<T> source)
        //    {
        //        SourceEumerable = source;
        //    }

        //    private IEnumerable<T> SourceEumerable { get; set; }
        //    private Enumeration _currentEnumeration;

        //    class Enumeration
        //    {
        //        public IEnumerator<T> Source { get; set; }
        //        public int Position { get; set; }
        //        public bool AtEnd { get; set; }
        //    }

        //    public bool Get(int pos, out T item)
        //    {

        //        if (_currentEnumeration != null && _currentEnumeration.Position > pos)
        //        {
        //            _currentEnumeration.Source.Dispose();
        //            _currentEnumeration = null;
        //        }

        //        if (_currentEnumeration == null)
        //            _currentEnumeration = new Enumeration { Position = -1, Source = SourceEumerable.GetEnumerator(), AtEnd = false };

        //        item = default(T);
        //        if (_currentEnumeration.AtEnd)
        //            return false;

        //        while (_currentEnumeration.Position < pos)
        //        {
        //            _currentEnumeration.AtEnd = !_currentEnumeration.Source.MoveNext();
        //            _currentEnumeration.Position++;

        //            if (_currentEnumeration.AtEnd)
        //                return false;
        //        }

        //        item = _currentEnumeration.Source.Current;
        //        return true;
        //    }

        //    private int _refs;

        //    public void AddRef()
        //    {
        //        _refs++;
        //    }

        //    public void RemoveRef()
        //    {
        //        _refs--;
        //        if (_refs != 0 || _currentEnumeration == null) return;
        //        var copy = _currentEnumeration;
        //        _currentEnumeration = null;
        //        copy.Source.Dispose();
        //    }
        //}

        //public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        //{
        //    if (chunksize < 1) throw new InvalidOperationException();

        //    var wrapper = new EnumeratorWrapper<T>(source);

        //    var currentPos = 0;
        //    try
        //    {
        //        wrapper.AddRef();
        //        T ignore;
        //        while (wrapper.Get(currentPos, out ignore))
        //        {
        //            yield return new ChunkedEnumerable<T>(wrapper, chunksize, currentPos);
        //            currentPos += chunksize;
        //        }
        //    }
        //    finally
        //    {
        //        wrapper.RemoveRef();
        //    }
        //}
    }
}
